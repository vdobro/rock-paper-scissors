package com.dobrovolskis.ropasc

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.27
 */
fun addGameStatistic(statistics: GameStatistics, result: GameResult): GameStatistics = when (result) {
	GameResult.DRAW -> statistics.copy(draws = statistics.draws + 1)
	GameResult.WIN -> statistics.copy(victories = statistics.victories + 1)
	GameResult.LOSE -> statistics.copy(opponentVictories = statistics.opponentVictories + 1)
}

fun countStatistics(games: List<GameRound>): GameStatistics {
	return games.map { it.result }
		.fold(GameStatistics(0, 0, 0)) { acc, gameResult -> addGameStatistic(acc, gameResult) }
}
