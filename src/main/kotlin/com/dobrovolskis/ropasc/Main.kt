package com.dobrovolskis.ropasc

import kotlin.random.Random

enum class GameAction {
	ROCK,
	PAPER,
	SCISSORS
}

fun pickRandomOption(): GameAction {
	val values = GameAction.values()
	return values[Random.nextInt(values.size)]
}

data class GameRoundInput(
	val mainPlayer: GameAction,
	val opponent: GameAction,
)

data class GameRound(
	val input: GameRoundInput,
	val result: GameResult,
)

data class GameStatistics(
	val victories: Int,
	val opponentVictories: Int,
	val draws: Int
)

const val GAME_COUNT = 100

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.26
 */
fun main() {
	val games = playGames(GAME_COUNT)
	val statistics = countStatistics(games)
	println("Player won ${statistics.victories}, " +
			"lost to opponent ${statistics.opponentVictories} " +
			"and tied ${statistics.draws} times")
}
