package com.dobrovolskis.ropasc

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.27
 */
fun isGameWon(gameInput: GameRoundInput): GameResult {
	val (mainPlayer, opponent) = gameInput
	if (mainPlayer == opponent) {
		return GameResult.DRAW
	}
	val victory = when (mainPlayer) {
		GameAction.ROCK -> opponent == GameAction.SCISSORS
		GameAction.PAPER -> opponent == GameAction.ROCK
		GameAction.SCISSORS -> opponent == GameAction.PAPER
	}
	return if (victory) GameResult.WIN else GameResult.LOSE
}

enum class GameResult {
	DRAW,
	WIN,
	LOSE
}
