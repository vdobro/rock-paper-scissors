package com.dobrovolskis.ropasc

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.27
 */
fun playGames(count: Int): List<GameRound> = IntRange(1, count).map { iteration ->
	val game = playRandomGame()
	val (input, result) = game
	val (me, opponent) = input
	println("Game $iteration: $me vs $opponent: $result")
	return@map game
}

private val DEFAULT_OPPONENT_ACTION: GameAction = GameAction.ROCK

private fun playRandomGame(): GameRound {
	val random = pickRandomOption()
	val staticPlayer = DEFAULT_OPPONENT_ACTION
	val gameInput = GameRoundInput(mainPlayer = random, opponent = staticPlayer)
	val result = isGameWon(gameInput)
	return GameRound(input = gameInput, result = result)
}
