package com.dobrovolskis.ropasc

import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.26
 */
class PlayerShould {

	@Test
	fun drawWhenEqual() {
		val options = GameAction.values()
		options.forEach { option ->
			val input = GameRoundInput(option, option)
			val output = isGameWon(input)
			assertEquals(output, GameResult.DRAW)
		}
	}

	private fun assertGameResult(
		player: GameAction,
		opponent: GameAction,
		result: GameResult
	) {
		val input = GameRoundInput(player, opponent)
		val output = isGameWon(input)
		assertEquals(output, result)
	}

	private fun assertVictory(game: GameAction, opponent: GameAction) {
		assertGameResult(game, opponent, GameResult.WIN)
	}

	private fun assertLoss(game: GameAction, opponent: GameAction) {
		assertGameResult(game, opponent, GameResult.LOSE)
	}

	@Test
	fun winWithRockAgainstScissors() {
		assertVictory(GameAction.ROCK, GameAction.SCISSORS)
	}

	@Test
	fun winWithPaperAgainstRock() {
		assertVictory(GameAction.PAPER, GameAction.ROCK)
	}

	@Test
	fun winWithScissorsAgainstPaper() {
		assertVictory(GameAction.SCISSORS, GameAction.PAPER)
	}

	@Test
	fun loseWithRockAgainstPaper() {
		assertLoss(GameAction.ROCK, GameAction.PAPER)
	}

	@Test
	fun loseWithPaperAgainstScissors() {
		assertLoss(GameAction.PAPER, GameAction.SCISSORS)
	}

	@Test
	fun loseWithScissorsAgainstRock() {
		assertLoss(GameAction.SCISSORS, GameAction.ROCK)
	}
}
