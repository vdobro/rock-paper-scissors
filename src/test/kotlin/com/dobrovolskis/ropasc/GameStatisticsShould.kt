package com.dobrovolskis.ropasc

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.27
 */
class GameStatisticsShould {

	@Test
	fun countZeroGames() {
		assertEquals(countStatistics(emptyList()), GameStatistics(0, 0, 0))
	}

	@Test
	fun addVictoryToStatistics() {
		val previous = createRandomStatistics()
		val expected = previous.copy(victories = previous.victories + 1)
		val calculated = addGameStatistic(previous, GameResult.WIN)
		assertEquals(expected, calculated)
	}

	@Test
	fun addLossToStatistics() {
		val previous = createRandomStatistics()
		val expected = previous.copy(opponentVictories = previous.opponentVictories + 1)
		val calculated = addGameStatistic(previous, GameResult.LOSE)
		assertEquals(expected, calculated)
	}

	@Test
	fun addDrawToStatistics() {
		val previous = createRandomStatistics()
		val expected = previous.copy(draws = previous.draws + 1)
		val calculated = addGameStatistic(previous, GameResult.DRAW)
		assertEquals(expected, calculated)
	}

	@Test
	fun sumToTheTotalAmountOfGames() {
		val gameCount = Random.nextInt(1, 42)
		val games = playGames(gameCount)
		val (wins, losses, draws) = countStatistics(games)
		val sum = wins + losses + draws
		assertEquals(sum, gameCount)
	}

	private fun createRandomStatistics(): GameStatistics =
		GameStatistics(Random.nextInt(), Random.nextInt(), Random.nextInt())
}
