package com.dobrovolskis.ropasc

import kotlin.test.Test
import kotlin.test.assertEquals

private const val TEST_GAME_COUNT = 42

/**
 * @author Vitalijus Dobrovolskis
 * @since 2022.09.27
 */
class GameLauncherShould {

	@Test
	fun playExactlyXGames() {
		IntRange(0, TEST_GAME_COUNT).forEach { gameCount ->
			val games = playGames(gameCount)
			assertEquals(games.size, gameCount)
		}
	}

	@Test
	fun evaluateGamesCorrectly() {
		val games = playGames(TEST_GAME_COUNT)
		games.forEach { round ->
			val (input, result) = round
			assertEquals(result, isGameWon(input))
		}
	}

}
